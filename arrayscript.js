function firstpick() {
    var first = ["a logo", "a badge", "a set of 5 icons", "website", "a UI", "a booking site", "a android app", "a VR website", "promotional fliers", "a packaging label", "a lower-thirds mockups", "an event ticker", "a desktop app", "a thank-you card", "a font", "a login dialog", "a Christmas card", "6 icons", "a set of stickers", "website wireframes", "an app", "a banner ad", "a mascot", "a management app", "a business card", "branded stickers", "a payment dialog", "4 animated icons", "a 404 page"];
    var show = first[Math.floor(Math.random() * first.length)];
    document.getElementById("pick1").innerHTML = show;
}
function secondpick() {
    var second = ["a bar", "your local sports team", "a theme park", "a shopping park", "a chain of VR restaurants", "Ubisoft", "an adventure tour company", "Spiderman", "the Post Office", "a bank in Singapore", "Canada", "a t-shirt brand", "a fast food chain", "Jeep", "a cafe in San Francisco", "a restaurant in Hong Kong", "your favorite football team", "a cooking show", "a hotel", "the Premier League", "the Olympics", "a space cruise ship", "a hovercraft company", "a Bitcoin wallet", "an orbital fueling station", "a remote controlled car", "a magic shop", "Tesla"];
    var show = second[Math.floor(Math.random() * second.length)];
    document.getElementById("pick2").innerHTML = show;
}

function onLoadCalc() {
    var amountOfFirst = first.length;
    var amountOfSecond = second.length;
    var show = amountOfFirst * amountOfSecond;
    document.getElementById("possibleComb").innerHTML = show;
} 
