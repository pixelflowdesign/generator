# GetInspired!

Hi! My name's Dan, and I like graphic design. I've never used JavaScript before. Well, that was, until I made this!


# What is it?

GetInspired is a utility to stop you from getting bored with your designs! Just press Generate, and go!

## How it's made!

This was made in around 3 hours by a guy brand-new to the world of Javascript; me! It was firstly made in Brackets, then the finishing touches made in Adobe Dreamweaver CC 2018.

## Always adding more!
Thanks to the world of GitHub, I'm always changing the site! If you have a suggestion, leave an issue!

## Where to contact me

If you want to contact me, leave an issue!

## License

This repository is open-sourced for the public and is protected under the Apache License. All Rights Reserved.
If you want to edit, or help me with something, fork this repository, then give us a pull request!

## Thanks!
made with <3 by pixelflow

